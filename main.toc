\babel@toc {english}{}
\contentsline {part}{\@mypartnumtocformat {I}{General information}}{5}{part.1}%
\contentsline {chapter}{\numberline {1}Welcome messages}{6}{chapter.1}%
\contentsline {section}{\numberline {1.1}Message from the EOU President}{6}{section.1.1}%
\contentsline {section}{\numberline {1.2}Message from local organizers}{6}{section.1.2}%
\contentsline {chapter}{\numberline {2}Organisers}{8}{chapter.2}%
\contentsline {section}{\numberline {2.1}Local Organising Committee}{8}{section.2.1}%
\contentsline {section}{\numberline {2.2}Scientific Programme Committee}{8}{section.2.2}%
\contentsline {chapter}{\numberline {3}Related events}{9}{chapter.3}%
\contentsline {part}{\@mypartnumtocformat {II}{Programme}}{10}{part.2}%
\contentsline {chapter}{\numberline {4}Monday, 26th August, 2019}{11}{chapter.4}%
\contentsline {section}{\numberline {4.1}Plenary}{11}{section.4.1}%
\contentsline {chapter}{\numberline {5}Tuesday, 27th August, 2019}{12}{chapter.5}%
\contentsline {section}{\numberline {5.1}Plenary}{12}{section.5.1}%
\contentsline {section}{\numberline {5.2}Parallel symposia I}{12}{section.5.2}%
\contentsline {section}{\numberline {5.3}Plenary}{14}{section.5.3}%
\contentsline {section}{\numberline {5.4}Parallel oral sessions I}{15}{section.5.4}%
\contentsline {chapter}{\numberline {6}Wednesday, 28th August, 2019}{25}{chapter.6}%
\contentsline {section}{\numberline {6.1}Plenary}{25}{section.6.1}%
\contentsline {section}{\numberline {6.2}Parallel symposia II}{25}{section.6.2}%
\contentsline {section}{\numberline {6.3}Plenary}{28}{section.6.3}%
\contentsline {section}{\numberline {6.4}Parallel oral sessions II}{28}{section.6.4}%
\contentsline {chapter}{\numberline {7}Thursday, 29th August, 2019}{38}{chapter.7}%
\contentsline {section}{\numberline {7.1}Excursion}{38}{section.7.1}%
\contentsline {chapter}{\numberline {8}Friday, 30th August, 2019}{39}{chapter.8}%
\contentsline {section}{\numberline {8.1}Plenary}{39}{section.8.1}%
\contentsline {section}{\numberline {8.2}Parallel oral sessions III}{39}{section.8.2}%
\contentsline {section}{\numberline {8.3}Plenary}{42}{section.8.3}%
\contentsline {section}{\numberline {8.4}Parallel oral sessions IV}{42}{section.8.4}%
\contentsline {part}{\@mypartnumtocformat {III}{Abstracts}}{46}{part.3}%
\contentsline {chapter}{\numberline {9}Plenaries}{47}{chapter.9}%
\contentsline {chapter}{\numberline {10}Symposia}{50}{chapter.10}%
\contentsline {chapter}{\numberline {11}Oral sessions}{75}{chapter.11}%
\contentsline {chapter}{\numberline {12}Posters}{138}{chapter.12}%
\contentsline {part}{\setlength \fboxsep {0pt}\noindent \colorbox {eougreen!40}{\strut \parbox [c][.7cm]{\linewidth }{\Large \sffamily \centering INDEX\hskip 1em\relax \mbox {}}}}{215}{section*.423}%
\contentsline {chapter}{\leavevmode {\color {eougreen}Author Index}}{216}{section*.424}%
\contentsfinish 
