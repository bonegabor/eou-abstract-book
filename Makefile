
# to make the programme and abstract book type `make` at the command
# line in the directory where the R and tex files are

book: EOU2019_abstracts.pdf

leaflet: EOU2019_leaflet.pdf

EOU2019_abstracts.pdf: main.pdf
	cp -a main.pdf EOU2019_abstracts.pdf

EOU2019_leaflet.pdf: leaflet.pdf
	pdftk leaflet.pdf cat 1-2 4-end output x.pdf
	pdftops x.pdf -| psnup -2 > EOU2019_leaflet.ps
	ps2pdf EOU2019_leaflet.ps
	rm -rf x.pdf EOU2019_leaflet.ps

leaflet.pdf: leaflet.tex leaflet_programme.tex structure.tex
	pdflatex leaflet.tex
	pdflatex leaflet.tex

leaflet_programme.tex: programme.tex
	sed -e '/pageref.entry/d' programme.tex > leaflet_programme.tex

main.pdf: main.tex abstracts.tex programme.tex structure.tex
	#pdflatex main.tex
	pdflatex main.tex
	makeindex main.idx -s StyleInd.ist
	pdflatex main.tex
	pdflatex main.tex
	pdflatex main.tex

abstracts.tex: abstracts.R functions.R
	Rscript abstracts.R

programme.tex: programme.R functions.R 
	Rscript programme.R

.PHONY: book leaflet

.INTERMEDIATE: ~/tmp/sessionList.xls ~/tmp/abstractlist.xls
