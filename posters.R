
# sort posters and pitchings

source("functions.R")

## read data

abs.df <- read.abstracts()

i <- abs.df$Type == "Poster" | abs.df$Type == "Pitching"

p <- abs.df[i,]

p.auth <- iconv(p$All.authors..last..first., from="UTF8",
								to="ascii//TRANSLIT")

p <- p[order(p.auth),]
p.auth <- p.auth[order(p.auth)]
p.no <- 1:length(p.auth)
day <- abs((p.no %% 2) - 2)

counter <- 0
for(i in 2:nrow(p)) {
	p1 <- i-1
	p2 <- i
	if(p$Type[p1] == "Pitching" & day[p1] == 2 & p$Type[p2] == "Poster" &
		 substr(p.auth[p1], 1, 3) == substr(p.auth[p2], 1, 3)) {
		#cat(p$Serial.No.[p1], p$Type[p1], day[p1], substr(p.auth[p1], 1, 10), "\n")
		#cat(p$Serial.No.[p2], p$Type[p2], day[p2], substr(p.auth[p2], 1, 10), "\n")
		z <- p.no[p1]
		p.no[p1] <- p.no[p2]
		p.no[p2] <- z
		counter <- counter + 1
	}
	if(counter == 2) break
}
p <- p[order(p.no),]
p.no <- 1:length(p.auth)
day <- abs((p.no %% 2) - 2)

#cat(paste(p$Serial.No., "\n", sep=""), file="poster_order.txt")

abs.df <- read.abstracts()

i <- abs.df$Type == "Poster" | abs.df$Type == "Pitching"

p <- abs.df[i,]

posters <- sort.posters()

for(i in names(posters)) {
	print(i)
	cat(paste(posters[[i]], "\n", sep=""))
}

tt <- seq(40, 68, 2) %% 60

sapply(posters, function(z) {y <- cbind(z, tt); y[order(z),]})
